# 11020STAT262200 統計資料分析 Statistical Data Analysis

## [Syllabus](Syllabus.pdf)

## Text Books
* ### Course materials

## References
* ### Online
    * #### [R Cookbook, 2nd Edition](https://rc2e.com/index.html)
    * #### [Cookbook for R](http://www.cookbook-r.com/)
    * #### [R Graphics Cookbook, 2nd edition](https://r-graphics.org/)
    * #### [R 資料科學與統計](https://bookdown.org/jefflinmd38/r4biost/)

* ### Books
    * #### [R in Action, Second Edition: Data analysis and graphics with R](References/R_in_Action_Second_Edition-Data_analysis_and_graphics_with_R.pdf)
    * #### [R Cookbook Proven Recipes for Data Analysis,Statistics, and Graphics SECOND EDITION](References/R_Cookbook_SECOND_EDITION.pdf)
    * #### [R Graphics Cookbook, 2nd edition](References/r-graphics-cookbook_2nd.epub)

## Homework
