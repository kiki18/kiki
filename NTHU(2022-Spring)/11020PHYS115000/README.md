# 11020PHYS115000 普通物理一(物理系) General Physics (I)

## [Syllabus](Syllabus.pdf)

## Text Books
* ### Lecture Notes

## References
1. ### [Fundamentals of Physics, Extended, 11th Edition](References/Fundamentals_of_Physics_Extended_11th-Edition.pdf)
2. ### [The Feynman Lectures on Physics](https://feynmanlectures.caltech.edu/)
    * #### [The Feynman Lectures on Physics, Vol. I_ The New Millennium Edition_ Mainly Mechanics, Radiation, and Heat](References/The_Feynman_Lectures_on_Physics_Vol.I_The_New_Millennium_Edition.pdf)
    * #### [The Feynman Lectures on Physics, Vol. II_ The New Millennium Edition_ Mainly Electromagnetism and Matter](References/The_Feynman_Lectures_on_Physics_Vol.II_The_New_Millennium_Edition.pdf)
    * #### [The Feynman Lectures on Physics, Vol. III_ The New Millennium Edition_ Quantum Mechanics](References/The_Feynman_Lectures_on_Physics_Vol.III_The_New_Millennium_Edition.pdf)

## Homework
