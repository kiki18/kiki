# 11020PHYS114402 普通物理Ａ二 General Physics A (II)

## [Syllabus](Syllabus.pdf)
* ### [Additional Information](Syllabus/11020PHYS114402.pdf)

## Text Books
* ### Lecture Notes

## References
1. ### [Fundamentals of Physics, Extended, 11th Edition](../11020PHYS115000/References/Fundamentals_of_Physics_Extended_11th-Edition.pdf)
2. ### [Principles of Physics A CALCULUS-BASED TEXT Raymond A. Serway John W. Jewett, Jr. FIFTH EDITION](References/Principles_of_Physics(A-Calculus-Based-Text)_Fifth_Edition.pdf)
3. ### [Physics for Scientists and Engineers with Modern Physics Ninth Edition](References/Physics_for_Scientists_and_Engineers_with_Modern_Physics_Ninth_Edition.pdf)
4. ### [Physics for Scientists and Engineers with Modern Physics tenth Edition](References/Physics_for_Scientists_and_Engineers_with_MODERN_PHYSICS_10TH_EDITION.pdf)

## Homework
1. ### [Homework 01](Homework/HW-01)
