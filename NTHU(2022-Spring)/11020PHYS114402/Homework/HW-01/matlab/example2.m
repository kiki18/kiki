%-------------------------------------------------------------------------% 
%
%     Computing a two-dimensional sketch of the equipotential lines and 
%     electric field lines of an electric dipole consisting of a pair of 
%     opposite charges of magnitude Q using Coulomb's Law.
%
%-------------------------------------------------------------------------%
clear; 
close all; 
clc;
%-------------------------------------------------------------------------%
%                             INITIALIZATION                                  
%-------------------------------------------------------------------------%
% Constant 1/(4*pi*epsilon_0) = 9*10^9
k = 9*10^9;
% eps_r = Relative permittivity
eps_r = 1;
charge_order = 10^-9; % milli, micro, nano etc..
const = k*charge_order/eps_r;
% Nx = Number of grid points in X- direction
% Ny = Number of grid points in Y-Direction
Nx = 101; % For 1 meter
Ny = 101; % For 1 meter
% n = Number of charges
n = 2;
% Electric fields Initialization
% E = Total electric field
E_f = zeros(Nx,Ny);
% Ex = X-Component of Electric-Field
% Ey = Y-Component of Electric-Field
Ex = E_f;
Ey = E_f;
% ex = unit vector for x-component electric field
% ey = unit vector for y-component electric field
ex = E_f;
ey = E_f;
% r = distance between a selected point and the location of charge
r = E_f;
r_square = E_f;
% Array of charges
% Q = All the 'n' charges are stored here
Q = [1,-1];
% Array of locations
X = [10,-10];
Y = [0,0];
%-------------------------------------------------------------------------%
%                      COMPUTATION OF ELECTRIC FIELDS
%-------------------------------------------------------------------------%
%  Repeat for all the 'n' charges
for k = 1:n
    q = Q(k);
    
    % Compute the unit vectors
    for i=1:Nx
        for j=1:Ny
            r_square(i,j) = (i-51-X(k))^2+(j-51-Y(k))^2;
            r(i,j) = sqrt(r_square(i,j));
            ex(i,j) = ex(i,j)+(i-51-X(k))./r(i,j);
            ey(i,j) = ey(i,j)+(j-51-Y(k))./r(i,j);
        end
    end
    
    E_f = E_f + q.*const./r_square;
    Ex = Ex + E_f.*ex.*const;
    Ey = Ex + E_f.*ey.*const;
end
%-------------------------------------------------------------------------%
%                           PLOT THE RESULTS
%-------------------------------------------------------------------------%
x_range = (1:Nx)-51;
y_range = (1:Ny)-51;
contour_range = -10:0.01:10;
contour(x_range,y_range,E_f',contour_range,'linewidth',0.5);
axis([-15 15 -15 15]);
xlabel('x','fontsize',14);
ylabel('y','fontsize',14);
title('Electric Dipole Distribution','fontsize',14);