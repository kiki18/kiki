## NTHU 2022 Spring (110下)
* ### [11020PHYS115000 普通物理一(物理系) General Physics (I)](NTHU(2022-Spring)/11020PHYS115000)
* ### [11020PHYS114402 普通物理Ａ二 General Physics A (II)](NTHU(2022-Spring)/11020PHYS114402)
* ### [11020STAT262200 統計資料分析 Statistical Data Analysis](NTHU(2022-Spring)/11020STAT262200)
* ### [11020IPTH100400 產品設計表現技法 Sketching for Product Design](NTHU(2022-Spring)/11020IPTH100400)
* ### [11020IPTH100600 平面設計 Graphics Design](NTHU(2022-Spring)/11020IPTH100600)
* ### [11020GEC 130401 藝術與社會 Art and Society 核心通識Core GE courses 3](NTHU(2022-Spring)/11020GEC-130401)
* ### [11020MS 531900 碳足跡與科技減碳 Carbon Footprint and Carbon Reduction](NTHU(2022-Spring)/11020MS-531900)
* ### [11020PE 206098 國際標準舞 Ballroom Dancing](NTHU(2022-Spring)/11020PE-206098)

| Section | MON | TUE | WED | THR | FRI | SAT |
|:-------:|:---:|:---:|:---:|:---:|:---:|:---:|
|1| | [普通物理一](NTHU(2022-Spring)/11020PHYS115000) | | | [普通物理一](NTHU(2022-Spring)/11020PHYS115000) | |
|2| | [普通物理一](NTHU(2022-Spring)/11020PHYS115000) | | | [普通物理一](NTHU(2022-Spring)/11020PHYS115000) | [產品設計表現技法](NTHU(2022-Spring)/11020IPTH100400) |
|3| | [普通物理Ａ二](NTHU(2022-Spring)/11020PHYS114402) | | [普通物理Ａ二](NTHU(2022-Spring)/11020PHYS114402) | |[產品設計表現技法](NTHU(2022-Spring)/11020IPTH100400) |
|4| | [普通物理Ａ二](NTHU(2022-Spring)/11020PHYS114402) | | [普通物理Ａ二](NTHU(2022-Spring)/11020PHYS114402) | |[產品設計表現技法](NTHU(2022-Spring)/11020IPTH100400) |
|n| | | [藝術與社會](NTHU(2022-Spring)/11020GEC-130401) | | | |
|5| [平面設計](NTHU(2022-Spring)/11020IPTH100600) | [統計資料分析](NTHU(2022-Spring)/11020STAT262200) | [藝術與社會](NTHU(2022-Spring)/11020GEC-130401) | [統計資料分析](NTHU(2022-Spring)/11020STAT262200) | | |
|6| [平面設計](NTHU(2022-Spring)/11020IPTH100600) | | [藝術與社會](NTHU(2022-Spring)/11020GEC-130401) | [統計資料分析](NTHU(2022-Spring)/11020STAT262200) | | |
|7| [平面設計](NTHU(2022-Spring)/11020IPTH100600) | | | [國際標準舞](NTHU(2022-Spring)/11020PE-206098) | | |
|8| | | [碳足跡與科技減碳](NTHU(2022-Spring)/11020MS-531900) |[國際標準舞](NTHU(2022-Spring)/11020PE-206098) | | |
|9| | | [碳足跡與科技減碳](NTHU(2022-Spring)/11020MS-531900) | | | |
|a| | | [碳足跡與科技減碳](NTHU(2022-Spring)/11020MS-531900) | | | |
